import 'package:flutter/material.dart';

class Radiowidget extends StatefulWidget {
  Radiowidget({Key? key}) : super(key: key);

  @override
  _RadiowidgetState createState() => _RadiowidgetState();
}

class _RadiowidgetState extends State<Radiowidget> {
  int val = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Radio'),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Radio(
              groupValue: val,
              value: 1,
              onChanged: (int? value) {
                setState(() {
                  val = value!;
                });
              },
            ),
            title: Text('One'),
          ),
          ListTile(
            leading: Radio(
              groupValue: val,
              value: 2,
              onChanged: (int? value) {
                setState(() {
                  val = value!;
                });
              },
            ),
            title: Text('Two'),
          ),
          ListTile(
            leading: Radio(
              groupValue: val,
              value: 3,
              onChanged: (int? value) {
                setState(() {
                  val = value!;
                });
              },
            ),
            title: Text('Three'),
          ),
          TextButton.icon(
            icon: Icon(Icons.save),
            label: Text('Save'),
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text('Select $val'),
              ));
            },
          ),
        ],
      ),
    );
  }
}
