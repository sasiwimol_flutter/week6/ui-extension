import 'package:flutter/material.dart';

class CheckBoxTilewidget extends StatefulWidget {
  CheckBoxTilewidget({Key? key}) : super(key: key);

  @override
  _CheckBoxTilewidgetState createState() => _CheckBoxTilewidgetState();
}

class _CheckBoxTilewidgetState extends State<CheckBoxTilewidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('comboBoxTile'),
        ),
        body: ListView(
          children: [
            CheckboxListTile(
                title: Text('check 1'),
                subtitle: Text('Sub check 1'),
                value: check1,
                onChanged: (value) {
                  setState(() {
                    check1 = value!;
                  });
                }),
            CheckboxListTile(
                title: Text('check 2'),
                subtitle: Text('Sub check 2'),
                value: check2,
                onChanged: (value) {
                  setState(() {
                    check2 = value!;
                  });
                }),
            CheckboxListTile(
                title: Text('check 3'),
                subtitle: Text('Sub check 3'),
                value: check3,
                onChanged: (value) {
                  setState(() {
                    check3 = value!;
                  });
                }),
            TextButton.icon(
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(
                        'check1: $check1, check2: $check2, check3: $check3'),
                  ));
                },
                icon: Icon(Icons.save),
                label: Text('Save')),
          ],
        ),
      ),
    );
  }
}
